package nl.sogeti;

/*
 * Copyright 2013 Red Hat, Inc.
 *
 * Red Hat licenses this file to you under the Apache License, version 2.0
 * (the "License"); you may not use this file except in compliance with the
 * License.  You may obtain a copy of the License at:
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations
 * under the License.
 *
 * @author <a href="http://tfox.org">Tim Fox</a>
 */

import nl.sogeti.config.HttpServerConfig;
import nl.sogeti.config.MatcherConfig;
import nl.sogeti.config.MongoConfig;
import nl.sogeti.handler.HistoryHandler;
import nl.sogeti.handler.SaveMessageHandler;
import nl.sogeti.service.DBService;

import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.http.HttpServer;
import org.vertx.java.core.http.HttpServerRequest;
import org.vertx.java.core.http.RouteMatcher;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Verticle;

/*
 *
 */
public class MainVerticle extends Verticle {

	public void start() {
		MongoConfig.deployModule(container, vertx);
		RouteMatcher matcher = MatcherConfig.getStaticRoute("index.html", "app");
		HttpServer server = vertx.createHttpServer().requestHandler(matcher);

		JsonArray noPermitted = new JsonArray();
		noPermitted.add(new JsonObject());
		vertx.createSockJSServer(server).bridge(new JsonObject().putString("prefix", "/eventbus"), noPermitted, noPermitted);

		vertx.eventBus().registerHandler("chat", new SaveMessageHandler(vertx, container));
		vertx.eventBus().registerHandler("history", new HistoryHandler(vertx, container));

		matcher.get("/api/history", new Handler<HttpServerRequest>() {

			@Override
			public void handle(final HttpServerRequest request) {
				new DBService(vertx).listMessages(new Handler<Message<JsonObject>>() {

					@Override
					public void handle(Message<JsonObject> result) {
						JsonArray messages = result.body().getArray("results");
						request.response().end(messages.toString());
					}
				});

			}
		});

		HttpServerConfig.startServer(container, server);

	}

}
