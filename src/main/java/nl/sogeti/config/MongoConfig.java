package nl.sogeti.config;

import java.util.UUID;

import nl.sogeti.handler.SaveMessageHandler;

import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Handler;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Container;


public class MongoConfig {
	
	public static final String ADDRESS = "mp2";
	
	
	public static JsonObject getConfig(final Container container) {
		JsonObject mongoConfig = new JsonObject();
		mongoConfig.putString("address", ADDRESS);
		String host = container.env().get("OPENSHIFT_MONGODB_DB_HOST");
		mongoConfig.putString("host", host != null ? host : "localhost");
		String port = container.env().get("OPENSHIFT_MONGODB_DB_PORT");
		mongoConfig.putNumber("port", port != null ? Integer.valueOf(port) : 27017);
		mongoConfig.putString("username", container.env().get("OPENSHIFT_MONGODB_DB_USERNAME"));
		mongoConfig.putString("password", container.env().get("OPENSHIFT_MONGODB_DB_PASSWORD"));
		mongoConfig.putString("db_name", "vertxtest");
		return mongoConfig;
	}
	
	public static void deployModule(final Container container, final Vertx vertx){
		container.deployModule("io.vertx~mod-auth-mgr~2.0.0-final");
		container.deployModule("io.vertx~mod-mongo-persistor~2.1.0",
				getConfig(container), new Handler<AsyncResult<String>>() {
					@Override
					public void handle(AsyncResult<String> arg0) {
						container.logger().info("deployed mongo module");
						vertx.eventBus().send(
								ADDRESS,
								new JsonObject().putString("action", "command").putString("command", "{createIndexes: 'message',indexes: [{key: {date: 1, name: 1, text: 1},name: 'date_name_text',unique: true}]}"), new Handler<Message<JsonObject>>() {
									
									@Override
									public void handle(Message<JsonObject> message) {
										// TODO Auto-generated method stub
										container.logger().info(message.body());
									}
								});
						String channel = UUID.randomUUID().toString();
						vertx.eventBus().registerHandler(channel, new SaveMessageHandler(vertx, container));
						vertx.eventBus().publish("history", new JsonObject().putString("channel", channel));
					}
				});
	}
}
