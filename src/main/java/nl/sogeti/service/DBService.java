package nl.sogeti.service;

import nl.sogeti.config.MongoConfig;

import org.vertx.java.core.Handler;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;

public class DBService {

	private Vertx vertx;

	public DBService(Vertx vertx) {
		this.vertx = vertx;
	}

	public void listMessages(Handler<Message<JsonObject>> handler) {
		JsonObject command = new JsonObject().putString("action", "find").putString("collection", "message").putObject("matcher", new JsonObject());

		this.vertx.eventBus().send(MongoConfig.ADDRESS, command, handler);
	}
}
