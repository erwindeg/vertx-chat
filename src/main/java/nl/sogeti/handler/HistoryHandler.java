package nl.sogeti.handler;

import nl.sogeti.service.DBService;

import org.vertx.java.core.Handler;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Container;

public class HistoryHandler implements Handler<Message<JsonObject>> {

	private Vertx vertx;

	public HistoryHandler(Vertx vertx, Container container) {
		this.vertx = vertx;
	}

	@Override
	public void handle(Message<JsonObject> event) {
		final String channel = event.body().getString("channel");
		new DBService(vertx).listMessages(new Handler<Message<JsonObject>>() {
			@Override
			public void handle(Message<JsonObject> result) {
				JsonArray messages = result.body().getArray("results");

				for (Object message : messages) {
					vertx.eventBus().send(channel, message);
				}

			}
		});

	}

}
