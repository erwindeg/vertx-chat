package nl.sogeti.handler;

import nl.sogeti.config.MongoConfig;

import org.vertx.java.core.Handler;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Container;

public class SaveMessageHandler implements Handler<Message<JsonObject>> {

	private Vertx vertx;
	private Container container;

	public SaveMessageHandler(Vertx vertx, Container container) {
		this.vertx = vertx;
		this.container = container;
	}

	@Override
	public void handle(final Message<JsonObject> event) {
		container.logger().info("message received: " + event.body());
		JsonObject command = new JsonObject();
		command.putString("action", "save").putString("collection", "message").putObject("document", new JsonObject(event.body().toString()));
		vertx.eventBus().send(MongoConfig.ADDRESS, command, new Handler<Message<JsonObject>>() {

			@Override
			public void handle(Message<JsonObject> message) {
				container.logger().info(message.body());
			}
		});
	}

}
